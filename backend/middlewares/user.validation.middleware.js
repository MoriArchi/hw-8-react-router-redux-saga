const { user } = require('../models/user');
const { checkRequiredFields, validateFields, checkErrors } = require('../helpers/middlewareHelper');

const createUserValid = (req, res, next) => {
  try {
    const errors = [];
    const fields = req.body;

    checkRequiredFields(fields, user, errors);
    validateFields(fields, errors);
    checkErrors(errors);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

const updateUserValid = (req, res, next) => {
  try {
    const errors = [];
    const fields = req.body;

    validateFields(fields, errors);
    checkErrors(errors);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
