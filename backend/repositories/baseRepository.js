const { dbAdapter } = require('../config/db');
const { v4 } = require('uuid');

class BaseRepository {
  constructor(collectionName) {
    this.dbContext = dbAdapter.get(collectionName);
    this.collectionName = collectionName;
  }

  generateId() {
    return v4();
  }

  getAll() {
    return this.dbContext.value();
  }

  getOne(search) {
    return this.dbContext.find(search).value();
  }

  create(data) {
    data.id = this.generateId();
    data.createdAt = new Date().toISOString();
    data.updatedAt = '';
    const list = this.dbContext.push(data).write();
    return list.find(it => it.id === data.id);
  }

  update(id, dataToUpdate) {
    const data = { ...dataToUpdate, updatedAt: new Date().toISOString() };
    return this.dbContext
      .find({ id })
      .assign(data)
      .write();
  }

  delete(id) {
    return this.dbContext.remove({ id }).write();
  }
}

exports.BaseRepository = BaseRepository;
