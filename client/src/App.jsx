import React from 'react';
import { Switch, Route } from 'react-router-dom';

import PrivateRoute from './shared/components/PrivateRoute';
import AdminRoute from './shared/components/AdminRoute';

import LoginPage from './containers/LoginPage';
import ChatPage from './containers/ChatPage';
import MessagePage from './containers/MessagePage';
import UsersPage from './containers/UsersPage';
import UserPage from './containers/UserPage';

function App() {
  return (
    <div>
      <Switch>
        <Route path="/login" component={LoginPage} />
        <PrivateRoute path="/chat" component={ChatPage} />
        <PrivateRoute path="/message/:id" component={MessagePage} />
        <AdminRoute path="/users" component={UsersPage} />
        <AdminRoute path="/user/:id" component={UserPage} />
        <AdminRoute path="/user" component={UserPage} />
        <Route component={LoginPage} />
      </Switch>
    </div>
  );
}

export default App;
