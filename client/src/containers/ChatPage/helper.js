import moment from 'moment';

const TIME_FORMAT = 'YYYY-MM-DD HH:mm:ss.SSSZ';

export const createMessagesSortedByDate = data => {
  if (!data) {
    return [];
  }

  const messages = data.map(message => {
    const [date, time] = getDate(message.createdAt);
    const isEdited = message.editedAt !== '';

    return {
      ...message,
      isEdited,
      date,
      time
    };
  });
  
  return messages
    .sort(({ createdAt }, { createdAt: secondCreatedAt }) => {
      return moment(createdAt, TIME_FORMAT).diff(secondCreatedAt, TIME_FORMAT);
    });
};

export const getPartipiantsCount = messages => messages.length
  ? new Set(messages.map(message => message.user)).size
  : 0;

export const getMessagesCount = messages => messages.length;

export const getLastMessageTime = messages => messages.length
  ? moment(messages[messages.length - 1].createdAt, TIME_FORMAT).format('Do MMMM HH:mm')
  : 'no messages';

export const getUserLastMessage = (messages, userId) => {
  const allUserMessages = messages.filter(message => message.userId === userId);
  if (allUserMessages.length) {

    const { id, text } = allUserMessages.slice(-1)[0];

    return { isMessageExist: true, id, text };
  }

  return { isMessageExist: false, id: '', text: '' };
};

export const checkIsLiked = (likes, userId) => {
  return likes.find(id => id === userId);
};

export const toggleMessageReact = (likes, userId) => {
  const isLike = checkIsLiked(likes, userId);
  if (isLike) {
    return likes.filter(id => id !== userId);
  }
  likes.push(userId);
  console.log(likes);
  return likes;
};

export const getDate = isoDate => {
  const date = moment(isoDate).format('ddd, MMM Do');
  const time = moment(isoDate).format('HH:mm');
  
  return [date, time];
}
