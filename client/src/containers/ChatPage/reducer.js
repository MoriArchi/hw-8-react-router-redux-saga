import { FETCH_MESSAGES, FETCH_MESSAGES_ERROR, FETCH_MESSAGES_SUCCESS } from './actionTypes';

const initialState = {
  isLoading: true,
  error: false,
  data: []
};

export default function(state = initialState, {type, payload}) {
  switch (type) {
    case FETCH_MESSAGES: {
      return { ...state, isLoading: true };
    }
      
    case FETCH_MESSAGES_ERROR: {
      return {
        isLoading: false,
        error: payload.error,
        data: []
      };
    }
      
    case FETCH_MESSAGES_SUCCESS: {
      return {
        isLoading: false,
        error: false,
        data: payload.data
      };
    }

    default:
      return state;
  }
}
