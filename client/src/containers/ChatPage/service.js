import { getReq, deleteReq, postReq, putReq } from '../../shared/helpers/requestHelper';
import { createMessagesSortedByDate } from './helper';

export const getMessages = async () => {
  const data = await getReq('messages');
  return createMessagesSortedByDate(data);
};

export const removeMessage = async id => {
  return await deleteReq('messages/', id);
};

export const createMessage = async message => {
  return await postReq('messages', message);
};

export const changeMessage = async (id, updatedMessage) => {
  return await putReq('messages', id, updatedMessage);
};
