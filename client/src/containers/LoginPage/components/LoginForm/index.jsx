import React, { useState } from 'react';
import TextInput from '../../../../shared/components/TextInput';

const LoginForm = props => {
  const [login, setLogin] = useState('');
  const [password, setPassword] = useState('');

  const onLoginChange = ({ target }) => {
    const value = target.value;
    setLogin(value);
  };

  const onPasswordChange = ({ target }) => {
    const value = target.value;
    setPassword(value);
  };

  const isDisabled = !login.trim().length || !password.trim().length;

  const onSubmit = event => {
    event.preventDefault();
    props.loginUser({ login, password });
  };

  return (
    <form className="login__form" onSubmit={onSubmit}>
      <TextInput
        type={'text'}
        onChange={onLoginChange}
        text={login}
        placeHolder="Type your login there..."
        label={'Login (username)'}
      />
      <TextInput
        type={'password'}
        onChange={onPasswordChange}
        text={password}
        placeHolder="Type your password there..."
        label={'Password'}
      />
      <button type="submit" disabled={isDisabled}>
        Log In
      </button>
    </form>
  );
};

export default LoginForm;
