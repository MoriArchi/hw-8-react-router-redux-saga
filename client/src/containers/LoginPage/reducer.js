import { LOGIN_USER, LOGIN_USER_ERROR, LOGIN_USER_SUCCESS, LOG_OUT } from './actionTypes';

const initialState = {
  isLoading: false,
  error: false,
  data: null
};

export default function(state = initialState, { type, payload }) {
  switch (type) {
    case LOGIN_USER: {
      return { ...state, error: false, isLoading: true };
    }
      
    case LOGIN_USER_ERROR: {
      return {
        ...state,
        isLoading: false,
        error: payload.error
      };
    }
      
    case LOGIN_USER_SUCCESS: {
      return {
        ...state,
        error: false,
        isLoading: false,
        data: payload.data
      };
    }
      
    case LOG_OUT: {
      return {
        ...state,
        isLoading: false,
        error: false,
        data: null
      };
    }

    default:
      return state;
  }
}
