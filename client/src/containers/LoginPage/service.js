import { postReq } from '../../shared/helpers/requestHelper';

export const login = async body => {
  return await postReq('auth/login', body);
};
