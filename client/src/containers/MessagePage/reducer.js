import { FETCH_MESSAGE, FETCH_MESSAGE_ERROR, FETCH_MESSAGE_SUCCESS } from './actionTypes';

const initialState = {
  isLoading: true,
  error: false,
  data: {}
};

export default function(state = initialState, { type, payload }) {
  switch (type) {
    case FETCH_MESSAGE: {
      return { ...state, error: false, isLoading: true };
    }
      
    case FETCH_MESSAGE_ERROR: {
      return {
        ...state,
        isLoading: false,
        error: payload.error
      };
    }
      
    case FETCH_MESSAGE_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        error: false,
        data: payload.data
      };
    }

    default:
      return state;
  }
}
