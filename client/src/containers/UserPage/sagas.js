import { call, put, takeEvery, all } from 'redux-saga/effects';
import {
  FETCH_USER,
  FETCH_USER_ERROR,
  FETCH_USER_SUCCESS,
  ADD_USER,
  ADD_USER_ERROR,
  ADD_USER_SUCCESS,
  UPDATE_USER,
  UPDATE_USER_ERROR,
  UPDATE_USER_SUCCESS
} from './actionTypes';
import { getUser, createUser, changeUser } from './service';

export function* fetchUser(action) {
  const { id } = action.payload;
  try {
    const user = yield call(getUser, id);
    yield put({ type: FETCH_USER_SUCCESS, payload: { data: user } });
  } catch (error) {
    yield put({ type: FETCH_USER_ERROR, payload: { error: error.message } });
  }
}

function* watchFetchUser() {
  yield takeEvery(FETCH_USER, fetchUser);
}

export function* addUser(action) {
  try {
    const { newUser } = action.payload;
    yield call(createUser, newUser);
    yield put({ type: ADD_USER_SUCCESS });
  } catch (error) {
    yield put({ type: ADD_USER_ERROR, payload: { error: error.message } });
  }
}

function* watchAddUser() {
  yield takeEvery(ADD_USER, addUser);
}

export function* updateUser(action) {
  try {
    const { id, data } = action.payload;
    yield call(changeUser, id, data);
    yield put({ type: UPDATE_USER_SUCCESS });
  } catch (error) {
    yield put({ type: UPDATE_USER_ERROR, payload: { error: error.message } });
  }
}

function* watchUpdateUser() {
  yield takeEvery(UPDATE_USER, updateUser);
}

export default function* userSagas() {
  yield all([watchFetchUser(), watchAddUser(), watchUpdateUser()]);
}
