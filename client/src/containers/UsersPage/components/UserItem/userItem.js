import React from 'react';

import './userItem.css';

export const UserItem = props => {
  const { avatar, name, onDelete, onUpdate } = props;
  return (
    <div className="user-item">
      <div className="user-item__item">
        <img src={avatar} alt={name} />
      </div>
      <div className="user-item__item">
        <p>{name}</p>
      </div>
      <div className="user-item__item">
        <i className="fa fa-cog" aria-hidden="true" onClick={onUpdate} />
      </div>
      <div className="user-item__item">
        <i className="fa fa-trash" aria-hidden="true" onClick={onDelete} />
      </div>
    </div>
  );
};
