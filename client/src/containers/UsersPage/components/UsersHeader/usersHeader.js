import React from 'react';

import './usersHeader.css';

export const UsersHeader = () => {
  return (
    <header className="users-header">
      <div className="users-header__item">
        <p>Avatar</p>
      </div>
      <div className="users-header__item">
        <p>Name</p>
      </div>
      <div className="users-header__item">
        <p>Edit</p>
      </div>
      <div className="users-header__item">
        <p>Delete</p>
      </div>
    </header>
  );
};
