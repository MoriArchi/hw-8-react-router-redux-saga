import { FETCH_USERS, FETCH_USERS_ERROR, FETCH_USERS_SUCCESS } from './actionTypes';

const initialState = {
  isLoading: true,
  error: false,
  data: []
};

export default function(state = initialState, { type, payload }) {
  switch (type) {
    case FETCH_USERS: {
      return { ...state, error: false, isLoading: true };
    }
      
    case FETCH_USERS_ERROR: {
      return {
        ...state,
        isLoading: false,
        error: payload.error
      };
    }
      
    case FETCH_USERS_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        error: false,
        data: payload.data
      };
    }

    default:
      return state;
  }
}
