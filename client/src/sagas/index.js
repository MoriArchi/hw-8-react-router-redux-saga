import { all } from 'redux-saga/effects';
import loginPageSagas from '../containers/LoginPage/sagas';
import chatPageSagas from '../containers/ChatPage/sagas';
import messagePageSagas from '../containers/MessagePage/sagas';
import usersPageSagas from '../containers/UsersPage/sagas';
import userPageSagas from '../containers/UserPage/sagas';

export default function* rootSaga() {
  yield all([loginPageSagas(), chatPageSagas(), messagePageSagas(), usersPageSagas(), userPageSagas()]);
}
