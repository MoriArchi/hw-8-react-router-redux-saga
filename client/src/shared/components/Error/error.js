import React, { useState } from 'react';

import './error.css';

export const Error = props => {
  const { message, onModalClose } = props;
  const [isOpen, setIsOpen] = useState(true);

  const onClose = () => {
    onModalClose && onModalClose();
    setIsOpen(false);
  };

  return (
    <div className={isOpen ? 'error-modal-container error-modal-container--show' : 'error-modal-container'}>
      <div className="error-modal-wrapper">
        <div className="error-modal">
          <h3 className="error-modal__header">Error!</h3>
          <div className="error-modal__body">
            <p className="error-modal__message">{message}</p>
            <button className="error-modal__button" onClick={onClose}>
              Close
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
