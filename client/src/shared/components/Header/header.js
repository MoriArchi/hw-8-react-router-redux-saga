import React from 'react';
import { Link } from 'react-router-dom';

import logo from '../../assets/logo.svg';
import './header.css';

export const Header = props => {
  const {
    logOut,
    profile: { avatar, isAdmin }
  } = props;
  return (
    <header className="header">
      <div className="header__container">
        <div className="header__left">
          <img className="header__logo" src={logo} alt="logo" />
        </div>
        <div className="header__links">
          {isAdmin  && (
            <>
              <Link to="/chat">Chat</Link>
              <Link to="/users">Users</Link>
            </>
          )}
        </div>
        <div className="header__right">
          <button onClick={logOut} className="header__logout">
            Log out
          </button>
          <img className="header__avatar" src={avatar} alt="avatar" />
        </div>
      </div>
    </header>
  );
};
