import React, { useState } from 'react';

import './success.css';

export const Success = props => {
  const { message, onModalClose } = props;
  const [isOpen, setIsOpen] = useState(true);

  const onClose = () => {
    onModalClose && onModalClose();
    setIsOpen(false);
  };

  return (
    <div className={isOpen ? 'success-modal-container success-modal-container--show' : 'success-modal-container'}>
      <div className="success-modal-wrapper">
        <div className="success-modal">
          <h3 className="success-modal__header">Success!</h3>
          <div className="success-modal__body">
            <p className="success-modal__message">{message}</p>
            <button className="success-modal__button" onClick={onClose}>
              Close
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
