import React, { useState } from 'react';
import './textInput.css';
import PropTypes from 'prop-types';

export const TextInput = props => {
  const [isValid, setIsValid] = useState(true);

  const { text, type, label, placeHolder, onChange } = props;

  const onBlur = event => {
    const isValid = event.target.value.trim().length;
    setIsValid(isValid);
  };

  const getErrorMessage = () => {
    return <span className="form__input__error">This field is required</span>;
  };

  const inputClass = isValid ? 'form__input' : 'form__input form__input--error';

  return (
    <div className="form__element">
      <label className="form__label ">{label}</label>
      {!isValid ? getErrorMessage() : null}
      <input value={text} type={type} className={inputClass} placeHolder={placeHolder} onChange={e => onChange(e)} onBlur={onBlur} />
    </div>
  );
};

TextInput.propTypes = {
  text: PropTypes.string,
  type: PropTypes.string,
  label: PropTypes.string,
  onChange: PropTypes.func
};
